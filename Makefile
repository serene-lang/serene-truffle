all:
	./gradlew clean
	./gradlew distZip

repl:
	./gradlew compileJava --warning-mode all && rlwrap java -cp build/classes/java/main/ serene.truffle.Main

run:
	./gradlew compileJava && java -cp build/classes/java/main/ serene.truffle.Main ${PWD}/test.srns

docs:
	npx docco src/**/*.java
clean:
	rm -v $$(find . -iname "*~")
	./gradlew clean
