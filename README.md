# Serene (truffle) version
**Serene truffle** is a lisp than is implemented in truffle as a proof of concept and as an experience
to collect data on some the ideas and implementations that I have. For the full story checkout
my [blog](https://lxsameer.com/).

## Requirements
* GraalVM
* rlwrap
* gradle

## Repl
in order to run the REPL, run `make repl`
