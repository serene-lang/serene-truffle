/**
 * Serene (truffle) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.truffle.builtin;

import java.util.List;

import serene.truffle.IScope;
import serene.truffle.SNumber;
import serene.truffle.SereneException;


public class GtFn extends AFn {
  public String fnName() {
    return ">";
  };

  public Object eval(IScope scope) throws SereneException{
    List<Object> args = this.arguments();

    if (args.size() == 0) {
      throw new SereneException(
        "ArityError: You need 1 or more parameters for this functions.");
    }

    if (args.size() == 1) {
      return true;
    }

    SNumber.IOps result = SNumber.createNumber(args.get(0));
    return result.gt(true, args.subList(1, args.size()));
  }
}
