/**
 * Serene (truffle) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.truffle;

import java.util.List;
import serene.truffle.SereneException;


public class SNumber {
  public static interface IOps {
    public IOps add(Object x);
    public IOps add(Double x);
    public IOps add(Long x);
    public IOps subtract(Object x);
    public IOps subtract(Double x);
    public IOps subtract(Long x);
    public IOps multiply(Object x);
    public IOps multiply(Double x);
    public IOps multiply(Long x);
    public IOps divide(Object x);
    public IOps divide(Double x);
    public IOps divide(Long x);
    public IOps mod(Object x);
    public IOps mod(Double x);
    public IOps mod(Long x);

    public boolean gt(boolean acc, List<Object> xs);
    public boolean gte(boolean acc, List<Object> xs);
    public boolean lt(boolean acc, List<Object> xs);
    public boolean lte(boolean acc, List<Object> xs);

    public Object value();
  }

  public abstract static class ANumber {



  }

  public static class LongNumber implements IOps  {
    private long v;

    public LongNumber(long v) {
      this.v = v;
    }

    public IOps add(Object x) throws SereneException {
      if (x instanceof Long) {
        return this.add((long) x);
      }
      else if (x instanceof Double) {
        return this.add((Double) x);
      }
      throw new SereneException("Can't cast anything beside Long and Double");
    }

    public IOps add(Long x) {
      return new LongNumber(this.v + x);
    }

    public IOps add(Double x) {
      DoubleNumber y = new DoubleNumber(x);
      return y.add(this.v);
    }

    public IOps subtract(Object x) throws SereneException {
      if (x instanceof Long) {
        return this.subtract((long) x);
      }
      else if (x instanceof Double) {
        return this.subtract((Double) x);
      }
      throw new SereneException("Can't cast anything beside Long and Double");
    }

    public IOps subtract(Long x) {
      return new LongNumber(this.v - x);
    }

    public IOps subtract(Double x) {
      DoubleNumber y = new DoubleNumber(x);
      return y.subtract(this.v);
    }

    public IOps multiply(Object x) throws SereneException {
      if (x instanceof Long) {
        return this.multiply((long) x);
      }
      else if (x instanceof Double) {
        return this.multiply((Double) x);
      }
      throw new SereneException("Can't cast anything beside Long and Double");
    }

    public IOps multiply(Long x) {
      return new LongNumber(this.v * x);
    }

    public IOps multiply(Double x) {
      DoubleNumber y = new DoubleNumber(x);
      return y.multiply(this.v);
    }

    public IOps divide(Object x) throws SereneException {
      if (x instanceof Long) {
        return this.divide((long) x);
      }
      else if (x instanceof Double) {
        return this.divide((Double) x);
      }
      throw new SereneException("Can't cast anything beside Long and Double");
    }

    public IOps divide(Long x) {
      return this.divide(x.doubleValue());
    }

    public IOps divide(Double x) {
      DoubleNumber y = new DoubleNumber(x);
      return y.divide(this.v);
    }

    public IOps mod(Object x) throws SereneException {
      if (x instanceof Long) {
        return this.mod((long) x);
      }
      else if (x instanceof Double) {
        return this.mod((Double) x);
      }
      throw new SereneException("Can't cast anything beside Long and Double");
    }

    public IOps mod(Long x) {
      return new LongNumber(this.v % x);
    }

    public IOps mod(Double x) {
      DoubleNumber y = new DoubleNumber(x);
      return y.mod(this.v);
    }

    public boolean gt(boolean acc, List<Object> xs) {
      for(Object x: xs) {
        if (!(x instanceof Number)) {
          throw new SereneException("Only number parameters can be use with '>' function.");
        }

        if (!(this.v > (x instanceof Long ? (long) x : (double) x))) {
          return false;
        }
      }

      return true;
    }

    public boolean gte(boolean acc, List<Object> xs) {
      for(Object x: xs) {
        if (!(x instanceof Number)) {
          throw new SereneException("Only number parameters can be use with '>' function.");
        }

        if (!(this.v >= (x instanceof Long ? (long) x : (double) x))) {
          return false;
        }
      }

      return true;
    }

    public boolean lt(boolean acc, List<Object> xs) {
      for(Object x: xs) {
        if (!(x instanceof Number)) {
          throw new SereneException("Only number parameters can be use with '>' function.");
        }

        if (!(this.v < (x instanceof Long ? (long) x : (double) x))) {
          return false;
        }
      }

      return true;
    }

    public boolean lte(boolean acc, List<Object> xs) {
      for(Object x: xs) {
        if (!(x instanceof Number)) {
          throw new SereneException("Only number parameters can be use with '>' function.");
        }

        if (!(this.v <= (x instanceof Long ? (long) x : (double) x))) {
          return false;
        }
      }

      return true;
    }

    public Object value() {
      return this.v;
    }
  }

  /**
   * Double number implementation.
   */
  public static class DoubleNumber  implements IOps  {
    private Double v;

    public DoubleNumber(Double v) {
      this.v = v;
    }

    public IOps add(Object x) throws SereneException {
      if (x instanceof Long) {
        return this.add((long) x);
      }
      else if (x instanceof Double) {
        return this.add((Double) x);
      }
      throw new SereneException("Can't cast anything beside Long and Double");
    }

    public IOps add(Double x) {
      return new DoubleNumber(this.v + x);
    }

    public IOps add(Long x) {
      return new DoubleNumber(this.v + x.doubleValue());
    }

    public IOps subtract(Object x) throws SereneException {
      if (x instanceof Long) {
        return this.subtract((long) x);
      }
      else if (x instanceof Double) {
        return this.subtract((Double) x);
      }
      throw new SereneException("Can't cast anything beside Long and Double");
    }

    public IOps subtract(Double x) {
      return new DoubleNumber(this.v - x);
    }

    public IOps subtract(Long x) {
      return new DoubleNumber(this.v - x.doubleValue());
    }

    public IOps multiply(Object x) throws SereneException {
      if (x instanceof Long) {
        return this.multiply((long) x);
      }
      else if (x instanceof Double) {
        return this.multiply((Double) x);
      }
      throw new SereneException("Can't cast anything beside Long and Double");
    }

    public IOps multiply(Double x) {
      return new DoubleNumber(this.v * x);
    }

    public IOps multiply(Long x) {
      return new DoubleNumber(this.v * x.doubleValue());
    }

    public IOps divide(Object x) throws SereneException {
      if (x instanceof Long) {
        return this.divide((long) x);
      }
      else if (x instanceof Double) {
        return this.divide((Double) x);
      }
      throw new SereneException("Can't cast anything beside Long and Double");
    }

    public IOps divide(Double x) {
      return new DoubleNumber(x / this.v);
    }

    public IOps divide(Long x) {
      return new DoubleNumber(x.doubleValue() / this.v);
    }

    public IOps mod(Object x) throws SereneException {
      if (x instanceof Long) {
        return this.mod((long) x);
      }
      else if (x instanceof Double) {
        return this.mod((Double) x);
      }
      throw new SereneException("Can't cast anything beside Long and Double");
    }

    public IOps mod(Long x) {
      return new DoubleNumber(this.v % x);
    }

    public IOps mod(Double x) {
      return new DoubleNumber(this.v % x);
    }

    public boolean gt(boolean acc, List<Object> xs) {
      for(Object x: xs) {
        if (!(x instanceof Number)) {
          throw new SereneException("Only number parameters can be use with '>' function.");
        }

        if (!(this.v > (x instanceof Long ? (long) x : (double) x))) {
          return false;
        }
      }

      return true;
    }

    public boolean gte(boolean acc, List<Object> xs) {
      for(Object x: xs) {
        if (!(x instanceof Number)) {
          throw new SereneException("Only number parameters can be use with '>' function.");
        }

        if (!(this.v >= (x instanceof Long ? (long) x : (double) x))) {
          return false;
        }
      }

      return true;
    }

    public boolean lt(boolean acc, List<Object> xs) {
      for(Object x: xs) {
        if (!(x instanceof Number)) {
          throw new SereneException("Only number parameters can be use with '>' function.");
        }

        if (!(this.v < (x instanceof Long ? (long) x : (double) x))) {
          return false;
        }
      }

      return true;
    }

    public boolean lte(boolean acc, List<Object> xs) {
      for(Object x: xs) {
        if (!(x instanceof Number)) {
          throw new SereneException("Only number parameters can be use with '>' function.");
        }

        if (!(this.v <= (x instanceof Long ? (long) x : (double) x))) {
          return false;
        }
      }

      return true;
    }

    public Object value() {
      return this.v;
    }
  }

  public static IOps createNumber(Object num) {
    if (num instanceof Long) {
      return new SNumber.LongNumber((long) num);
    }
    else if (num instanceof Double) {
      return new SNumber.DoubleNumber((Double) num);
    }
    throw new SereneException("Can't cast anything beside Long and Double");
  }
}
