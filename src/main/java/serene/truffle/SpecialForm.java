/**
 * Serene (truffle) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.truffle;


public class SpecialForm extends SereneNode {
  private static final SymbolNode DEF = new SymbolNode("def");
  private static final SymbolNode FN = new SymbolNode("fn");
  private static final SymbolNode IF = new SymbolNode("if");
  private static final SymbolNode QUOTE = new SymbolNode("quote");
  private static final SymbolNode LET = new SymbolNode("let");
  private static final SymbolNode COND = new SymbolNode("cond");
  private static final SymbolNode DO = new SymbolNode("do");

  public ListNode<SereneNode> node;

  public SpecialForm() {}

  public SpecialForm(ListNode<SereneNode> node) {
    this.node = node;
  }

  public static Node check(ListNode<SereneNode> l) throws SereneException {
    if (l == ListNode.EMPTY) {
      return l;
    } else if (l.first().equals(DEF)) {
      return new DefSpecialForm(l);
    } else if (l.first().equals(LET)){
      return new LetSpecialForm(l);
    } else if (l.first().equals(DO)){
      return new DoSpecialForm(l);
    } else if (l.first().equals(COND)){
      return new CondSpecialForm(l);
    } else if (l.first().equals(FN)) {
      return new FnSpecialForm(l);
    } else if (l.first().equals(IF)) {
      return new IfSpecialForm(l);
    } else if (l.first().equals(QUOTE)) {
      return new QuoteSpecialForm(l);
    }

    return l;
  }

  @Override
  public Object eval(IScope scope) throws SereneException {
    throw new SereneException("Can't use SpecialForm directly");
  }
}
