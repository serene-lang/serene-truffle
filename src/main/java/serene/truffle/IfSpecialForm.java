/**
 * Serene (truffle) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.truffle;


public class IfSpecialForm extends SpecialForm {
  private Node pred;
  private Node ifNode;
  private Node elseNode;

  public IfSpecialForm(ListNode<SereneNode> l) {
    super(l);
    this.pred = l.rest().first();
    this.ifNode = l.rest().rest().first();
    this.elseNode = l.rest().rest().rest().first();
  }

  @Override
  public Object eval(final IScope scope) {
    Object result = this.pred.eval(scope);
    if (result == null || (result instanceof Boolean && (Boolean) result == false)) {
      return this.elseNode.eval(scope);
    }
    return this.ifNode.eval(scope);
  }
}
