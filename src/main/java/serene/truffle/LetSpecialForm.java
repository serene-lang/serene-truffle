/**
 * Serene (truffle) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.truffle;

public class LetSpecialForm extends SpecialForm {
  private ListNode<SereneNode> bindings;
  private ListNode<SereneNode> body;

  public LetSpecialForm(ListNode<SereneNode> listNode) {
    super(listNode);

    if (!(listNode.rest().first() instanceof ListNode)) {
      throw new SereneException("Let bindings should be a list of pairs.");
    }

    this.bindings = (ListNode<SereneNode>) listNode.rest().first();
    this.body = listNode.rest().rest();
  }

  @Override
  public Object eval(IScope scope) {
    Scope letScope = new Scope(scope);

    for (Object x : this.bindings) {
      if (!(x instanceof ListNode))
        throw new SereneException(String.format("Expect a pair instead of '%s'", x.toString()));

      letScope.insertSymbol(
        ((ListNode<SereneNode>) x).first().toString(),
        ((ListNode<SereneNode>) x).rest().first().eval(letScope));
    }

    Object result = ListNode.EMPTY;

    for(Node x: body) {
      result = x.eval(letScope);
    }

    return result;
  }
}
